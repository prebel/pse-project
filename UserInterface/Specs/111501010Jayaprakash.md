# Indian Institute Of Technology, Palakkad

###                                  Principles of Software Engineering Lab Project

##### 									                                                                           UI TEAM



​																                                      Jayaprakash A
​                                                                                                                                                                       111501010			



**Objective**

- Server Side messaging and text processing UI elements design.
- Code the functions based on events using message processing API.



**Design**

- **Login** :

  ​	Google API was first chosen to be the mode of login authentication. But as the software needs to work on local network, this idea was dropped. The next idea was to use database and store the passwords of the users. But, even this is dropped. Finally there is no authentication. The user enters the username and is free to use the software. 

  ​	The below image is a sample UI design that shows the login method in the server side application.

  ![](./111501010Jayaprakash/BeforeLogin.png)

- **Server Side Chat Screen**:

  ​	The server side chat screen will be having the following utilities. Screen space for sending and receiving messages. This would be the central part of the screen. On the left hand side, there will be a panel which contains the list of all the clients arranged according to the most recent chat time. 

  ​	The next feature is the count of unread messages for each individual client and display them. 	The active client is also highlighted for better understanding. The below image is a sample UI design that shows the chat screen and other panels.

  ![](./111501010Jayaprakash/AfterLogin.png)



- **Transporting messages**

  ​	When messages are received create proper labels and populate them on the chat workspace. Align the other messages accordingly. When the server enters a message, take the data and give it to appropriate module.



##### Component wise description

1. **Login Button**: After entering username press login button to start conversation. This button now changes to signout button.
2. **Send Button:** After typing the message press the send button. This will send message to corresponding modules.
3. **Client Name**: On clicking a client name in the left hand side panel, the chat screen of the corresponding client would be opened.
4. **Unread Messages:** The small green circle at the bottom right of the client panel shows the number of unread messages of that client.
5. **Signout Button:** The user signs-out and is no longer eligible to send messages.



**Class Diagram**

​	The image shown below has all the classes involved and the interaction between them.

![](./111501010Jayaprakash/ServerClassDiagram.png)