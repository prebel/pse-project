# The MASTI Command Line Tool
_Prepared by Adrian McDonald Tariang, 111501001 on 12-09-2018_

## Implementation

### Details
- The objective of this executable is to accept the command line parameters
- Run required tools or test based on parameter selection
- Flag Handling can be taken care of with the [Command Line Parser Tool](https://github.com/commandlineparser/commandline)

### Flag Description
- `-t` or `--test NameTest`: run the named test or use `all` to execute all the test in the project
- `-v`, `-vv` or `-vvv`: specify the verbosity of logging
- `-o` or `--output outputFile.txt`: write the test output to a file
- `-r` or `--run NameTool`: run the specified Tool

### Usage
```
	# execute all the tests in the projects 
	> masti -t all

	# execute a test named MessageTest defined in your module
	> masti -t MessageTest

	# specify verbosity for output to Console
	> masti -t MessageTest -vv

	# execute a test and specify a file for output
	> masti -t MessageTest -o testOutput.txt 

	# specify verbosity for output in file
	> masti -t MessageTest -o testOutput.txt -vv

	# execute a tool named ExportMessagesTool
	> masti -r ExportMessagesTool -o messageOutput.txt
```