# Telemetry
_Prepared by Adrian McDonald Tariang, 111501001 on 12-09-2018_

## Implementation

### Details
- Allow the module developers to collect data from program execution
- The data collected should not contain information of the identity of the user and should be used to collect statistics to improve the software experience
- The Developers implement the `ITelemetry` interface to write their statistic calculations
- The Developers initialised their telemetry object and pass it to the `TelemetryCollection` Class, which will store collected data during the program termination phase
- The `TelemetryCollection` stores the Telemetry objects to have the module store their statistics in a consistant manner throughout the execution of the program
- The `TelemetryCollection` uses the Singleton Design Patterns, and stores instances of the `ITelemetry` objects for each of the modules
- This data can be encoded into JSON and stored with a naming pattern of `Telemetry[date][lognumber].json`

### Interface 
The Developers will implement the `ITelemetry` interface within their module. This class will contain dedicated functions for extracting the telemetry data and store it in the `dataCapture` object

```csharp
	public interface ITelemetry
    {
        // Hold the data that needs to be collected by the module.
        IDictionary<string, string> dataCapture { get; set; }
    }
```

Details on the `ITelemetry` Interface
- **dataCapture**: 
	- The String Key-Pair Dictionary where developers store the data to be collected
- **Calculate(object data)**: 
	- Can be used by the developers to define functions to extract their statistics
- **Summarise()**: 
	- Summarise captured data and store into `dataCapture`

The Interface of the TelemetryCollection class, This will be developed by the Tools and Validation Team and the module developers can use it in order to execute their Telemetry functions and store data

```csharp
	public interface ITelemetryCollection
    {
        // Holds the ITelemetry instances of each of the modules.
        IList<ITelemetry> registeredTelemetry { get; set; }

        // Return the telemetry object for a module.
        ITelemetry GetTelemetryObject(string telemetryObjectName);

        // Write the data within each of the registeredTelemetry objects to a file.
        bool StoreTelemetry();

        // Register the Telemetry object for a module referenced with the given string key.
        void RegisterTelemetry(string telemetryName, ITelemetry telemetryObject);
    }
```

Details on the `ITelemetryCollection` Interface  
- **registeredTelemetry**:  
	- Stores the Telemetry objects used by the modules in the project along with the data collected by them
	- Use by `GetTelemetryObject` function to retrive a `ITelemetry` for a particular module
	- Can also be used by the `StoreTelemetry` function to encode and store the collected data of each module
- **GetTelemetryObject(string telemetryObjectName)**:
	- returns Null if the `ITelemetry` does not exist in `registeredTelemetry`
	- Return the object stored in `registeredTelemetry` if it exists
- **StoreTelemetry()**: 
	- To be used during the program termination phase 
	- Collect all data from the `dataCapture` attributes of each of the `ITelemetry` objects
	- Serialise the data to JSON
	- Store to a file with the naming format `Telemetry[date][lognumber].json`

### Usage

The user will need to implement the `ITelemetry` interface in order to collect data
```csharp
// An Example Telemetry class for the Fruit module.
public class FruitTelemetry :ITelemetry
{
	IDictionary<string, string> dataCapture;

	int FindNewTotal(int quantity)
	{
		int total = Int32.Parse(dataCapture["total_fruits_sold"]);

		int newTotal = total + quantity;

		return newTotal;
	}

	public void Calculate(object fruitSold)
	{
		dataCapture["total_fruits_sold"] = FindNewTotal(fruitSold.quantity);
		...
	}
}
```

The developer can then use this object of this class within their module. Instantiation of the object is performed by the `TelemetryCollection` class
```csharp
// An Example Module feature
public class FruitStore 
{
	public boolean SellFruit(string fruitName, int soldQuantity) 
	{
		// storage of data is abstracted.
		ITelemetry fruitTelemetry = TelemetryCollection.GetTelemetryObject("FruitTelemetry");

		// If the Telemetry hasn't been registered.
		if (fruitTelemetry == null) {
			fruitTelemetry = new FruitTelemetry();

			// Registers the Telemetry Object into the TelemetryCollection Singleton.
			TelemetryCollection.RegisterTelemetry("FruitTelemetry", fruitTelemetry);
		}
		
		Fruit fruitSold = getFruitObject(fruitName);
		...

		fruitTelemetry.Calculate(fruitSold);

		if (success) {
			Diagnostics.LogSuccess("Transaction Success");
		} else {
			Diagnostics.LogError("Transaction Failed");
		}
	}
}
```

### Notes for Development
- JSON Serialisation can be performed using fuctions in the `System.Runtime.Serialization.Json` namespace provided by the .NET Framework.
- Similar to `Diagnostics`, `TelemetryCollection` uses the Singleton Design Pattern and will be accessable to all modules
