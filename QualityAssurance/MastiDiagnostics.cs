﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     6th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="MastiDiagnostics.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class provides logging facilities for the application. There 
//    are options to log different kinds of messages like Error, Info, 
//    Success, Warning. These will be held in memory and committed to file
//    when necessary so that file IO doesn't take up too much time. There
//    is also provision to mail logs of current session to a developer mail
//    which is pre-specified in a configuration file.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Net.Mail;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Threading.Tasks;
    using static CommandLineOptions;

    /// <summary>
    /// Class that handles diagnostics.
    /// </summary>
    public static class MastiDiagnostics
    {
#if DEBUG
        /// <summary>
        /// Check whether debug build.
        /// </summary>
        private const bool DebugMode = true;
#else
        private const bool DebugMode = false; 
#endif

        /// <summary>
        /// Specifies which type of messages have to be logged.
        /// </summary>
        private static IList<CommandLineOptions.LogMessageType> verbosityControls = new List<CommandLineOptions.LogMessageType>();

        /// <summary>
        /// An instance of FileLogger used to log Diagnostics data.
        /// </summary>
        private static FileLogger logger;

        /// <summary>
        /// Initializes static members of the <see cref="MastiDiagnostics"/> class.
        /// Determines level of verbosity and instantiates FileLogger with those values.
        /// </summary>
        static MastiDiagnostics()
        {
            // Check if Info needs to be logged.
            if (Properties.Settings.Default.logInfo)
            {
                verbosityControls.Add(CommandLineOptions.LogMessageType.Info);
            }

            // Check if Warning needs to be logged.
            if (Properties.Settings.Default.logWarning)
            {
                verbosityControls.Add(CommandLineOptions.LogMessageType.Warning);
            }

            // Check if Error needs to be logged.            
            if (Properties.Settings.Default.logError)
            {
                verbosityControls.Add(CommandLineOptions.LogMessageType.Error);
            }

            // Check if success needs to be logged.
            if (Properties.Settings.Default.logSuccess)
            {
                verbosityControls.Add(CommandLineOptions.LogMessageType.Success);
            }

            // Instantiate the FileLogger with these controls.
            logger = new FileLogger(verbosityControls);
        }

        /// <summary>
        /// Log Info type message.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        /// <param name="filePath">Path to file from which this method is called.</param>
        /// <param name="memberName">Name of method which calls this method.</param>
        public static void LogInfo(
            string message,
            [CallerFilePath] string filePath = "",
            [CallerMemberName] string memberName = "")
        {
            logger.LogInfo(AddCallerName(message, filePath, memberName));
        }

        /// <summary>
        /// Log Warning type message.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        /// <param name="filePath">Path to file from which this method is called.</param>
        /// <param name="memberName">Name of method which calls this method.</param>
        public static void LogWarning(
            string message,
            [CallerFilePath] string filePath = "",
            [CallerMemberName] string memberName = "")
        {
            logger.LogWarning(AddCallerName(message, filePath, memberName));
        }
        
        /// <summary>
        /// Log Error type message.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        /// <param name="filePath">Path to file from which this method is called.</param>
        /// <param name="memberName">Name of method which calls this method.</param>
        public static void LogError(
            string message,
            [CallerFilePath] string filePath = "",
            [CallerMemberName] string memberName = "")
        {
            logger.LogError(AddCallerName(message, filePath, memberName));

            // Write log to file when error occurs.
            WriteLog();
        }

        /// <summary>
        /// Log Success type message.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        /// <param name="filePath">Path to file from which this method is called.</param>
        /// <param name="memberName">Name of method which calls this method.</param>
        public static void LogSuccess(
            string message,
            [CallerFilePath] string filePath = "",
            [CallerMemberName] string memberName = "")
        {
            logger.LogSuccess(AddCallerName(message, filePath, memberName));
        }    
        
        /// <summary>
        /// Mail the logs to pre-specified e-mail id.
        /// </summary>
        public static void MailLog()
        {
            logger.MailLog();
        }

        /// <summary>
        /// Commit accumulated logs to file.
        /// </summary>
        public static void WriteLog()
        {
            logger.WriteLog();
        }

        /// <summary>
        /// If debug build, prepend caller class and method to the message.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        /// <param name="filePath">Path to file from which this method is called.</param>
        /// <param name="memberName">Name of method which calls this method.</param>
        /// <returns>Modified message.</returns>
        private static string AddCallerName(
            string message,
            string filePath = "",
            string memberName = "")
        {
            if (DebugMode)
            {
                // Get tokens from file path.
                string[] tokens = filePath.Split('\\');

                // Get name from path tokens.
                string fileName = tokens[tokens.Length - 1];

                // Remove file extension.
                fileName = fileName.Split('.')[0];

                return string.Format(CultureInfo.CurrentCulture, "[{0}.{1}] {2}", fileName, memberName, message);
            }
            else
            {
                return message;
            }
        }
    }
}
