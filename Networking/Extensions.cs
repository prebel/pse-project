﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal (29ayush@gmail.com)
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
// Rajat Sharma
// </reviewer>
//
// <copyright file="Extensions.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
//      You are allowed to use the file and/or redistribute/modify as long as you preserve this copyright header and author tag.
// </copyright>
//
// <summary>
//      Defines an extension Slice for array class.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;

    /// <summary>
    /// Add some functions to existing classes.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Get the array slice between the two indexes.
        /// ... Inclusive for start index, exclusive for end index.
        /// </summary>
        /// <typeparam name="T">Type of array</typeparam>
        /// <param name="source">Source array </param>
        /// <param name="start">Starting index</param>
        /// <param name="end">Ending index</param>
        /// <returns>The Slice of array inclusive of start index only.</returns>
        public static T[] Slice<T>(this T[] source, int start, int end)
        {
            // Handles negative ends.
            if (end < 0)
            {
                end = source.Length + end;
            }

            int len = end - start;
            if (len < 0) 
            {
                Diagnostics.LogError("Start Index too high");
                throw new InvalidOperationException();
            }

            // Return new array.
            T[] res = new T[len];
            for (int i = 0; i < len; i++)
            {
                res[i] = source[i + start];
            }

            return res;
        }
    }
}