﻿// -----------------------------------------------------------------------
//  <copyright file="Communication.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
//  </copyright>
//  <Module>Netwoking Module</Module>
// <Author>Libin</Author>
// <Author>Parth</Author>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System.Net;

    /// <summary>
    /// Communicator class implementing ICommunication Interface
    /// </summary>
    public partial class Communication : ICommunication
    {
        /// <summary>
        /// Method to send data transfer request.
        /// </summary>
        /// <param name="msg">Data to to be send</param>
        /// <param name="targetIP">Recepient IP</param>
        /// <param name="type">Will be used to find component that will notified for message status.</param>
        /// <returns>success status</returns>
        public bool Send(string msg, IPAddress targetIP, DataType type)
        {
            SendRequest request = new SendRequest(msg, targetIP, type);
            SendRequestQueue.Enqueue(request);
            this.queueEvent.Set();

            return true;
        }
    }
}
