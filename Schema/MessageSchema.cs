﻿//-----------------------------------------------------------------------
// <copyright file="MessageSchema.cs" company="B'15, IIT Palakkad">
//      Open Source. Feel free to use the code, but don't forget to acknowledge. 
// </copyright>
// <author>
//      Harsh Yadav
// </author>
// <review>
//      Libin N George
// </review>
//-----------------------------------------------------------------------

namespace Masti.Schema
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The following class implements the interface of ISchema for text message encoding and partial and/or full decoding
    /// </summary>
    public class MessageSchema : ISchema
    {
        /// <summary>
        /// Function used to parse string data to convert into JSON format
        /// </summary>
        /// <param name="data">Encoded string</param>
        /// <param name="partialDecoding">Flag used to partially decode data to get object type (ImageProcessing/Messaging)</param>
        /// <returns>Dictionary of tags of encoded data</returns>
        public IDictionary<string, string> Decode(string data, bool partialDecoding)
        {
            Dictionary<string, string> tagDict = new Dictionary<string, string>();
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            string[] keyValues = data.Split(';');
            ////for partial decoding returning the dictionary only containing type of data give (Image/ message /other)
            if (partialDecoding == true)
            {
                string[] type = keyValues[0].Split(':');

                // this was the first tag inserted into data while encoding
                // so inserting Type tag
                if (DecodeFrom64(type[0]) == "type")
                {
                    tagDict.Add(DecodeFrom64(type[0]), DecodeFrom64(type[1]));
                }
                else
                {
                    throw new System.ArgumentException("Data is corrupted as it does not contain type of data", nameof(data));
                }

                return tagDict;
            }

            // fully decoding if the partialDecoding is false and adding into the dictionary
            foreach (string keyValue in keyValues)
            {
                tagDict.Add(DecodeFrom64(keyValue.Split(':')[0]), DecodeFrom64(keyValue.Split(':')[1]));
            }

            tagDict.Remove("type");
            return tagDict;
        }

        /// <summary>
        /// Encodes data obtained from a dictionary to a string
        /// </summary>
        /// <param name="tagDict">Dictionary containing tag to be encoded</param>
        /// <returns>Encoded string of data from dictionary</returns>
        public string Encode(Dictionary<string, string> tagDict)
        {
            if (tagDict == null)
            {
                throw new ArgumentNullException(nameof(tagDict));
            }

            Dictionary<string, string>.KeyCollection keys = tagDict.Keys;
            string result = string.Empty;

            // inserting the Type tag into encoding data
            result = EncodeTo64("type") + ':' + EncodeTo64("Messaging") + ';';
            foreach (string key in keys)
            {
                result += EncodeTo64(key) + ':' + EncodeTo64(tagDict[key]) + ";";
            }

            return result.Substring(0, result.Length - 1);
        }

        /// <summary>
        /// Converts string to base64 Encoding
        /// </summary>
        /// <param name="toEncode">Data to encode into Base64</param>
        /// <returns>Encoded string</returns>
        private static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        /// <summary>
        /// Decodes the base64 encoded string
        /// </summary>
        /// <param name="encodedData">Data to decode into Base64</param>
        /// <returns>Decoded string from base64 to normal</returns>
        private static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }
    }
}
